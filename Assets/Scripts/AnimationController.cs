using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
        {
            // Toggle the boolean parameter in the Animator controller
            bool currentBoolState = animator.GetBool("IsWalking");
            animator.SetBool("IsWalking", !currentBoolState);
        }
        else
        {
            animator.SetBool("IsWalking", false);
        }

    }
}
