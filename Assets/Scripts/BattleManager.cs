using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;

public class BattleManager : MonoBehaviour
{
    public GameObject battleUI, choiceObj, guideObj, topObj, botObj;
    public TextMeshProUGUI pRollTxt, eRollTxt, highObj, orObj, lowObj, UpperTxt, LowerTxt;
    public Image pDiceObj, eDiceObj, vignette;
    public Sprite[] diceSprite;

    [Space]
    public Cinemachine.CinemachineVirtualCamera virtualCamera;

    enum phase { PlayerRolling, Guessing, Done }
    phase currentPhase;
    enum answer { High, Low }
    answer currentAnswer;

    bool canGuess, rolled, isRepeat;
    float inputValue;
    int lastValue;

    private Character[] characters;

    private void Start()
    {
        battleUI.SetActive(false);
        characters = FindObjectsOfType<Character>();
        currentPhase = phase.Done;
    }

    public void StartBattle(Character player, Character enemy)
    {
        Character[] characters = FindObjectsOfType<Character>();

        foreach (var character in characters)
        {
            character.enabled = false;
        }

        StartCoroutine(BattleCouroutine(player, enemy));
    }

    public void OnSelection(InputValue value)
    {

        if (canGuess)
        {
            //SFX HERE
            AudioManager.Instance.PlaySFX(AudioManager.Instance.selectSFX);
            inputValue = value.Get<float>();
            print("Guessing");

            switch (inputValue)
            {
                case 1:
                    currentAnswer = answer.High;
                    break;
                case -1:
                    currentAnswer = answer.Low;
                    break;
            }

            ChoiceSelectAnim((int)inputValue);
        }
    }

    public void OnConfirm()
    {
        switch (currentPhase)
        {
            case phase.PlayerRolling:
                rolled = true;
                //SFX HERE
                if (rolled || canGuess)
                {
                    Debug.Log("rolling");
                    AudioManager.Instance.PlaySFX(AudioManager.Instance.rollingDiceSFX);
                }


                break;
            case phase.Guessing:
                canGuess = false;
                //SFX HERE
                if (!canGuess || !rolled)
                {
                    Debug.Log("guesing");
                    AudioManager.Instance.PlaySFX(AudioManager.Instance.rollingDiceSFX);
                }

                break;
            case phase.Done:
                break;
        }

    }

    IEnumerator BattleCouroutine(Character player, Character enemy)
    {
        bool isBattling = true;
        AudioManager.Instance.PlayMusic(AudioManager.Instance.BattleMusic);

        player.changeState(Character.state.Battle);
        enemy.changeState(Character.state.Battle);
        int playerDiceValue = 0;
        int enemyDiceValue = 0;


        battleUI.SetActive(true);
        BattleStartAnim();

        while (isBattling)
        {
            var Delay = 0.1f;

            currentPhase = phase.PlayerRolling;

            PlayerRollPhaseEntryAnim();

            rolled = false;
            pRollTxt.text = "< Press Confirm\n  To Stop Rolling";

            while (currentPhase == phase.PlayerRolling)
            {
                playerDiceValue = RandomWithoutRepeat(1, 7);
                pDiceObj.sprite = diceSprite[playerDiceValue - 1];
                yield return new WaitForSeconds(Delay);

                if (rolled)
                {
                    pRollTxt.text = "WAIT A\nSEC...";

                    Delay += .1f;

                    if (Delay > 1f)
                    {
                        pRollTxt.text = "Player's\nDice";
                        pDiceObj.gameObject.LeanMoveLocalY(10, 1).setEasePunch();
                        pDiceObj.gameObject.LeanScale(Vector3.one * 1.25f, 1).setEasePunch();

                        currentPhase = phase.Guessing;
                    }
                }
            }

            //transition here if first time
            GuessingPhaseTransitionAnim();

            Delay = 0.1f;

            canGuess = true;

            while (currentPhase == phase.Guessing)
            {
                enemyDiceValue = RandomWithoutRepeat(1, 7);
                eDiceObj.sprite = diceSprite[enemyDiceValue - 1];
                yield return new WaitForSeconds(Delay);

                if (!canGuess)
                {
                    ChoiceConfirmAnim();

                    Delay += .1f;

                    if (Delay > 1f)
                    {

                        eRollTxt.text = "Enemy's\nDice";
                        eDiceObj.gameObject.LeanMoveLocalY(10, 1).setEasePunch();
                        eDiceObj.gameObject.LeanScale(Vector3.one * 1.25f, 1).setEasePunch();
                        break;
                    }
                }
            }


            //Compare Value After Confirmed
            switch (currentAnswer)
            {
                case answer.High: //Choose High
                    if (enemyDiceValue > playerDiceValue)
                    {
                        LowerTxt.text = "You <b>LIVE.";

                        player.changeState(Character.state.Alive);
                        enemy.changeState(Character.state.Die);

                        isBattling = false;
                        isRepeat = false;
                    }
                    else if (enemyDiceValue == playerDiceValue)
                    {
                        //repeat
                        isRepeat = true;
                    }
                    else
                    {
                        LowerTxt.text = "You <b>DIED.";

                        player.changeState(Character.state.Die);
                        enemy.changeState(Character.state.Alive);

                        isBattling = false;
                        isRepeat = false;
                    }
                    break;

                case answer.Low: //Choose Low
                    if (enemyDiceValue < playerDiceValue)
                    {
                        LowerTxt.text = "You <b>LIVE.";

                        player.changeState(Character.state.Alive);
                        enemy.changeState(Character.state.Die);

                        isBattling = false;
                        isRepeat = false;
                    }
                    else if (enemyDiceValue == playerDiceValue)
                    {
                        //repeat
                        isRepeat = true;
                    }
                    else
                    {
                        LowerTxt.text = "You <b>DIED.";

                        player.changeState(Character.state.Die);
                        enemy.changeState(Character.state.Alive);

                        isBattling = false;
                        isRepeat = false;
                    }
                    break;
            }

            // Countdown
            if (isRepeat)
            {
                LowerTxt.text = "<b>TIED.</b> Repeating in 3";
                yield return new WaitForSeconds(1);
                LowerTxt.text = "<b>TIED.</b> Repeating in 2";
                yield return new WaitForSeconds(1);
                LowerTxt.text = "<b>TIED.</b> Repeating in 1";
                BattleRepeatAnim();
                yield return new WaitForSeconds(1.5f);
            }
        }

        //End Battle
        yield return new WaitForSeconds(1f);

        BattleEndAnim();
        yield return new WaitForSeconds(1f);
        battleUI.SetActive(false);
        foreach (var item in characters)
        {
            item.enabled = true;
        }
    }


    int RandomWithoutRepeat(int incMin, int excMax)
    {

        int chosenInt = 0;

        chosenInt = Random.Range(incMin, excMax);

        while (chosenInt == lastValue)
        {
            print("Randomize Again");
            chosenInt = Random.Range(incMin, excMax);
        }

        lastValue = chosenInt;

        return chosenInt;
    }


    void BattleStartAnim()
    {
        //SFX HERE
        AudioManager.Instance.PlaySFX(AudioManager.Instance.openAndCloseBattleSFX);

        //init pos
        guideObj.transform.localPosition = new Vector3(guideObj.transform.localPosition.x, 380);
        topObj.transform.localPosition = Vector3.up * 250;
        botObj.transform.localPosition = Vector3.up * -250;

        guideObj.LeanMoveLocalY(-185, .8f).setEase(LeanTweenType.easeOutElastic);
        topObj.LeanMoveLocalY(205, .5f).setEase(LeanTweenType.easeOutCirc); ;
        botObj.LeanMoveLocalY(-205, .5f).setEase(LeanTweenType.easeOutCirc); ;

        LeanTween.value(9, 4, 1f).setOnUpdate((float val) => { virtualCamera.m_Lens.OrthographicSize = val; });
        vignette.CrossFadeAlpha(0, 0, false);
        vignette.CrossFadeAlpha(1, 1, false);


    }

    void BattleRepeatAnim()
    {
        //SFX HERE

        choiceObj.LeanMoveLocalY(500, 1f).setEase(LeanTweenType.easeOutQuad);
        pDiceObj.gameObject.LeanMoveLocalY(300, 1f).setEase(LeanTweenType.easeOutQuad);
        eDiceObj.gameObject.LeanMoveLocalY(300, 1f).setEase(LeanTweenType.easeOutQuad);
    }

    void BattleEndAnim()
    {
        //SFX HERE
        currentPhase = phase.Done;
        AudioManager.Instance.PlaySFX(AudioManager.Instance.openAndCloseBattleSFX);
        AudioManager.Instance.PlayMusic(AudioManager.Instance.backgroundMusic);

        guideObj.LeanMoveLocalY(guideObj.transform.localPosition.y + -200f, 1f).setEase(LeanTweenType.easeOutQuad);
        topObj.LeanMoveLocalY(topObj.transform.localPosition.y + 30, 1f).setEase(LeanTweenType.easeOutQuad);
        botObj.LeanMoveLocalY(botObj.transform.localPosition.y + -30, 1f).setEase(LeanTweenType.easeOutQuad);

        choiceObj.LeanMoveLocalY(500, 1f).setEase(LeanTweenType.easeOutQuad);
        pDiceObj.gameObject.LeanMoveLocalY(300, 1f).setEase(LeanTweenType.easeOutQuad);
        eDiceObj.gameObject.LeanMoveLocalY(300, 1f).setEase(LeanTweenType.easeOutQuad);

        vignette.CrossFadeAlpha(0, 1, false);
        LeanTween.value(4, 9, 1f).setOnUpdate((float val) => { virtualCamera.m_Lens.OrthographicSize = val; });
    }

    void PlayerRollPhaseEntryAnim()
    {
        UpperTxt.gameObject.SetActive(true);
        highObj.gameObject.SetActive(true);
        lowObj.gameObject.SetActive(true);
        orObj.gameObject.SetActive(true);

        highObj.transform.localPosition = (Vector2.up * 25);
        lowObj.transform.localPosition = (Vector2.up * -25);

        choiceObj.transform.localPosition += (Vector3.up * 500);
        pDiceObj.transform.localPosition = (Vector2.up * 300);
        eDiceObj.transform.localPosition += (Vector3.up * 300);

        pDiceObj.gameObject.LeanMoveLocal(Vector2.zero, 1f).setEaseOutBounce();
    }

    void GuessingPhaseTransitionAnim()
    {
        LowerTxt.text = "<b>...Than Yours.";
        eRollTxt.text = "ENEMY'S\nROLLING";

        pDiceObj.gameObject.LeanMoveLocalX(110, 1f).setEaseOutBounce();
        choiceObj.gameObject.LeanMoveLocalY(0, 1.15f).setEaseOutQuad();
        eDiceObj.gameObject.LeanMoveLocalY(0, 1f).setEaseOutBounce();
    }

    void ChoiceSelectAnim(int input)
    {
        var choice = gameObject;
        var choiceNotTaken = gameObject;

        switch (currentAnswer)
        {
            case answer.High:
                choice = highObj.gameObject;
                choiceNotTaken = lowObj.gameObject;
                break;
            case answer.Low:
                choice = lowObj.gameObject;
                choiceNotTaken = highObj.gameObject;
                break;
        }

        choice.LeanScale(Vector3.one * 1.1f, .1f).setEase(LeanTweenType.easeOutBounce);
        choiceNotTaken.LeanScale(Vector3.one * .9f, .1f).setEase(LeanTweenType.easeOutBounce);
    }

    void ChoiceConfirmAnim()
    {
        var choice = gameObject;
        var choiceNotTaken = gameObject;

        switch (currentAnswer)
        {
            case answer.High:
                choice = highObj.gameObject;
                choiceNotTaken = lowObj.gameObject;
                break;
            case answer.Low:
                choice = lowObj.gameObject;
                choiceNotTaken = highObj.gameObject;
                break;
        }

        choice.LeanMoveLocalY(0, .1f).setEase(LeanTweenType.easeOutBounce);
        choice.LeanScale(Vector3.one * 1.5f, .1f).setEase(LeanTweenType.easeOutBounce);

        UpperTxt.gameObject.SetActive(false);
        LowerTxt.gameObject.SetActive(true);
        choiceNotTaken.gameObject.SetActive(false);
        orObj.gameObject.SetActive(false);
    }
}
