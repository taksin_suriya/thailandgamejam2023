using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ControllerManager : MonoBehaviour
{
    [SerializeField] private ControllerState currentState = ControllerState.All;
    [SerializeField] private Image iconRed;
    [SerializeField] private Image iconGreen;
    [SerializeField] private Image iconPurple;//monster1
    [SerializeField] private Image iconAll;

    private void Start()
    {
        currentState = ControllerState.All;
    }

    private void Update()
    {
        // Check the current state and execute the corresponding logic.
        switch (currentState)
        {
            case ControllerState.All:
                SetPlayerInputEnabledByTag("Player", true);
                SetPlayerInputEnabledByTag("Monster_1", true);
                SetPlayerInputEnabledByTag("Monster_2", true);
                iconGreen.color = new Color(iconGreen.color.r, iconGreen.color.g, iconGreen.color.b, 0.5f);
                iconRed.color = new Color(iconRed.color.r, iconRed.color.g, iconRed.color.b, 0.5f);
                iconPurple.color = new Color(iconPurple.color.r, iconPurple.color.g, iconPurple.color.b, 0.5f);
                iconAll.color = new Color(iconPurple.color.r, iconPurple.color.g, iconPurple.color.b, 1);
                break;

            case ControllerState.Player:
                SetPlayerInputEnabledByTag("Player", true);
                SetPlayerInputEnabledByTag("Monster_1", false);
                SetPlayerInputEnabledByTag("Monster_2", false);
                iconGreen.color = new Color(iconGreen.color.r, iconGreen.color.g, iconGreen.color.b, 0.5f);
                iconRed.color = new Color(iconRed.color.r, iconRed.color.g, iconRed.color.b, 1f);
                iconPurple.color = new Color(iconPurple.color.r, iconPurple.color.g, iconPurple.color.b, 0.5f);
                iconAll.color = new Color(iconPurple.color.r, iconPurple.color.g, iconPurple.color.b, 0.5f);
                break;

            case ControllerState.Monster_1:
                SetPlayerInputEnabledByTag("Player", false);
                SetPlayerInputEnabledByTag("Monster_1", true);
                SetPlayerInputEnabledByTag("Monster_2", false);
                iconGreen.color = new Color(iconGreen.color.r, iconGreen.color.g, iconGreen.color.b, 0.5f);
                iconRed.color = new Color(iconRed.color.r, iconRed.color.g, iconRed.color.b, .5f);
                iconPurple.color = new Color(iconPurple.color.r, iconPurple.color.g, iconPurple.color.b, 1f);
                iconAll.color = new Color(iconPurple.color.r, iconPurple.color.g, iconPurple.color.b, 0.5f);
                break;

            case ControllerState.Monster_2:
                SetPlayerInputEnabledByTag("Player", false);
                SetPlayerInputEnabledByTag("Monster_1", false);
                SetPlayerInputEnabledByTag("Monster_2", true);
                iconGreen.color = new Color(iconGreen.color.r, iconGreen.color.g, iconGreen.color.b, 1f);
                iconRed.color = new Color(iconRed.color.r, iconRed.color.g, iconRed.color.b, .5f);
                iconPurple.color = new Color(iconPurple.color.r, iconPurple.color.g, iconPurple.color.b, .5f);
                iconAll.color = new Color(iconPurple.color.r, iconPurple.color.g, iconPurple.color.b, 0.5f);
                break;
        }
    }

    private void SetPlayerInputEnabledByTag(string tag, bool enabled)
    {
        GameObject[] objectsWithTag = GameObject.FindGameObjectsWithTag(tag);

        foreach (var obj in objectsWithTag)
        {
            GridMovement gridMovement = obj.GetComponent<GridMovement>();
            PlayerInput playerInput = obj.GetComponent<PlayerInput>();
            Recruitment recruitment = obj.GetComponent<Recruitment>();

            if (playerInput != null)
            {
                if (tag == "Player")
                {
                    gridMovement.enabled = enabled;
                }
                else if (tag == "Monster_1" && recruitment != null && recruitment.canMove)
                {
                    gridMovement.enabled = enabled;
                }
                else if (tag == "Monster_2" && recruitment != null && recruitment.canMove)
                {
                    gridMovement.enabled = enabled;
                }
            }
        }
    }

    public void SwitchToAllState()
    {
        currentState = ControllerState.All;
    }

    public void SwitchToPlayerState()
    {
        currentState = ControllerState.Player;
    }

    public void SwitchToMonster_1State()
    {
        currentState = ControllerState.Monster_1;
    }

    public void SwitchToMonster_2State()
    {
        currentState = ControllerState.Monster_2;
    }

    public void OnAllState()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.selectSFX);
        Debug.Log("OnAllState");
        SwitchToAllState();
    }

    public void OnPlayerState()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.selectSFX);
        Debug.Log("OnPlayerState");
        SwitchToPlayerState();
    }

    public void OnMonster1State()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.selectSFX);
        Debug.Log("OnMonster1State");
        SwitchToMonster_1State();
    }

    public void OnMonster2State()
    {
        AudioManager.Instance.PlaySFX(AudioManager.Instance.selectSFX);
        Debug.Log("OnMonster2State");
        SwitchToMonster_2State();
    }

    private enum ControllerState
    {
        All,
        Player,
        Monster_1,
        Monster_2
    }

}