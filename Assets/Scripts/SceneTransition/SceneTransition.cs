using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SceneTransition : MonoBehaviour
{
    // Serialize field makes these variables accessible in the Unity Inspector.
    [SerializeField] private Material screenTransitionMaterial; // The material used for screen transition effects.
    [SerializeField] private float transitionDuration = 1f;     // The duration of the transition effect in seconds.
    [SerializeField] string transitionName = "_TransValuePixel"; // The name of the transition parameter in the shader.

    public UnityEvent OnTransitionDone; // UnityEvent to invoke when the transition is done.
    public UnityEvent OnTransitionBeforeTransitionProgress;

    // This method is called when the object is created in the scene.
    private void Start()
    {
        // Start the transition effect immediately when the object is created.
        StartCoroutine(TransitionProgress());
    }

    // This method can be called from another script to trigger the transition effect.
    public void PlayTransition()
    {
        // Start a reverse transition effect (e.g., going back to the previous scene).
        StartCoroutine(BeforeTransitionProgress());
    }

    // Coroutine for progressing the transition effect.
    private IEnumerator TransitionProgress()
    {
        float currentTransitionDuration = 0;

        // Continue until the current transition duration reaches the specified duration.
        while (currentTransitionDuration < transitionDuration)
        {
            currentTransitionDuration += Time.deltaTime;
            
            // Update the transition parameter in the shader based on progress.
            screenTransitionMaterial.SetFloat(transitionName, Mathf.Clamp01(currentTransitionDuration / transitionDuration));
            
            // Yielding here allows the coroutine to pause and continue in the next frame.
            yield return null;
        }

        // Invoke the UnityEvent to signal that the transition is complete.
        OnTransitionDone?.Invoke();
    }

    // Coroutine for reversing the transition effect.
    private IEnumerator BeforeTransitionProgress()
    {
        float currentTransitionDuration = 1;

        // Continue until the current transition duration reaches the specified duration.
        while (currentTransitionDuration <= transitionDuration)
        {
            currentTransitionDuration -= Time.deltaTime;
            
            // Update the transition parameter in the shader based on progress.
            screenTransitionMaterial.SetFloat(transitionName, Mathf.Clamp01(currentTransitionDuration / transitionDuration));
            
            // Yielding here allows the coroutine to pause and continue in the next frame.
            yield return null;
        }

        // Invoke the UnityEvent to signal that the reverse transition is complete.
        OnTransitionBeforeTransitionProgress?.Invoke();
    }

    
}
