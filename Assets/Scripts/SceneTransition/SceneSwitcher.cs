using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    public GameObject hideButton;
    public SceneTransition sceneTransition; // Reference to the SceneTransition script for transition effects.
    public GameObject timeline;
    public GameObject audioObject;
    public GameObject playerObject;

    // Method to load the next scene, triggering a transition effect.
    public void LoadNextScene()
    {
        Debug.Log("NextScene");
        StartCoroutine(WaitToChangeScene());
    }

    // Method to load the previous scene, triggering a transition effect.
    public void ToPreviousScene()
    {
        Debug.Log("PreviousScene");
        StartCoroutine(WaitToChangePreviousScene());
    }

    public void SkipCutScene()
    {
        audioObject.SetActive(true);
        playerObject.SetActive(true);
        timeline.SetActive(false);

    }

    // Coroutine to delay scene loading and play a transition effect.
    IEnumerator WaitToChangeScene()
    {
        hideButton.SetActive(false);
        sceneTransition.PlayTransition(); // Play the transition effect.

        yield return new WaitForSeconds(3); // Wait for 1 second (adjust as needed).

        // Load the next scene based on the current scene's build index.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // Coroutine to delay loading the previous scene and play a transition effect.
    IEnumerator WaitToChangePreviousScene()
    {
        //hideButton.SetActive(false);

        sceneTransition.PlayTransition(); // Play the transition effect.

        yield return new WaitForSeconds(3); // Wait for 1 second (adjust as needed).

        // Load the previous scene based on the current scene's build index.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

}
