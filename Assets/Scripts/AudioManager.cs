using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance { get; private set; }

    [Header("Audio Source")]
    [SerializeField] private AudioSource musicSource;
    [SerializeField] private AudioSource SFXSource;

    [Header("Audio Clips")]
    public AudioClip backgroundMusic;
    public AudioClip endMusic;
    public AudioClip BattleMusic;
    public AudioClip footStepSFX;
    public AudioClip helpFriendSFX;
    public AudioClip rollingDiceSFX;
    public AudioClip playerStartCombatSFX;
    public AudioClip deathSFX;
    public AudioClip loseSFX;
    public AudioClip winSFX;
    public AudioClip selectSFX;
    public AudioClip openAndCloseBattleSFX;
    public AudioClip fightSFX;

    //AudioManager.Instance.PlaySFX(AudioManager.Instance.helpFriendSFX);

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        musicSource.clip = backgroundMusic;
        musicSource.Play();
    }

    public void PlaySFX(AudioClip clip)
    {
        if (clip != null)
        {
            SFXSource.PlayOneShot(clip);
        }
        else
        {
            Debug.LogError("Trying to play a null AudioClip.");
        }
    }

    public void PlayMusic(AudioClip music)
    {
        musicSource.clip = music;
        musicSource.Play();
    }
}
