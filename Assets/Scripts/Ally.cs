using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Ally : Character
{
    //public Rigidbody2D rb;
    public PlayerInput playerInput;
    //public Animator animator;
    public Renderer rend;

    private void Start()
    {
        // Get references to components in Start.
        rb = GetComponent<Rigidbody2D>();
        playerInput = GetComponent<PlayerInput>();
        animator = GetComponent<Animator>();
        rend = GetComponent<Renderer>();

        // Check if rend is not null before setting its color.
        if (rend != null)
        {
            rend.material.color = Color.red;
        }

        // Initialize other properties.
        transform.eulerAngles = new Vector3(0, 0, 90);
        rb.bodyType = RigidbodyType2D.Static;
        animator.enabled = false;
        //playerInput.enabled = false;
    }


    public void OnMoveUp()
    {
        Move(Vector3.up);
    }
    public void OnMoveDown()
    {
        Move(Vector3.down);
    }
    public void OnMoveLeft()
    {
        Move(Vector3.left);
    }
    public void OnMoveRight()
    {
        Move(Vector3.right);
    }


    protected override void OnStateChanged()
    {
        switch (currentSlate)
        {
            case state.Alive:
                break;
            case state.Die:
                //play dead anim then disable
                //SFX HERE
                Destroy(gameObject);
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Triggered by player");

            if (playerInput != null)
            {
                // Change the color to white.
                if (rend != null)
                {
                    rend.material.color = Color.white;
                }

                // Enable player input and update other properties.
                //playerInput.enabled = true;
                rb.bodyType = RigidbodyType2D.Dynamic;
                transform.eulerAngles = new Vector3(0, 0, 0);
                animator.enabled = true;
                //AudioManager.Instance.PlaySFX(AudioManager.Instance.helpFriendSFX);
            }
            else
            {
                Debug.LogWarning("PlayerInput component is missing.");
            }
        }
    }
}
