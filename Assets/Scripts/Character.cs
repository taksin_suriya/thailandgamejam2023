using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Character : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float gridSize = 1f; // Adjust this to match your grid cell size
    [Tooltip("Which layers this character can't walk into.")]
    public LayerMask stopMask;

    private Vector3 targetPosition, orignalPosition;
    protected bool isMoving = false;
    public Rigidbody2D rb;
    protected Animator animator;

    public enum state { Alive, Die, Battle, Down }
    public state currentSlate = state.Alive;

    private void Start()
    {
        targetPosition = transform.position;
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    protected virtual void Update()
    {

    }

    protected void Move(Vector3 direction)
    {
        if (isMoving || (currentSlate != state.Alive))
            return;

        Vector3 newPosition = transform.position + direction * gridSize;

        RaycastHit2D hit = Physics2D.Linecast(transform.position, newPosition, stopMask);

        if (hit.collider == null)
        {
            targetPosition = newPosition;
            StartCoroutine(MoveToTargetPosition(targetPosition));

            if (gameObject.tag == "Player")
            {
                AudioManager.Instance.PlaySFX(AudioManager.Instance.footStepSFX);

            }
        }
        else
        {
            Debug.Log("Hit something: " + hit.collider.name); // Log the collider name for debugging
        }

    }

    private IEnumerator MoveToTargetPosition(Vector3 target)
    {
        isMoving = true;

        Vector3 originalPosition = transform.position;
        float elapsedTime = 0f;

        while (elapsedTime < moveSpeed)
        {
            transform.position = Vector3.Lerp(originalPosition, target, elapsedTime / moveSpeed);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = target;
        isMoving = false;
    }

    public void changeState(state state)
    {
        currentSlate = state;
        OnStateChanged();
    }

    protected virtual void OnStateChanged()
    {

    }
}
