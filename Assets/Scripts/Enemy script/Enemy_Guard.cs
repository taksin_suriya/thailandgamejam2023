using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Guard : Enemy
{
    [Tooltip("����ö�Թ�Ũҡ�ش����������ҡ���˹")]
    public int DistanceLimit;
    int currentdistance;

    Vector3 startingPoint;

    protected override void Start()
    {
        base.Start();

        startingPoint = transform.position;
    }

    protected override void WalkPattern()
    {
        if (DistanceLimit == 0)
            return;

        // Guards walk in a random manner but will try to stay in their zone

        currentdistance = (int)(startingPoint - transform.position).magnitude;

        var newdistance = 0;

        List<Vector3> avalibleDirection = new List<Vector3>();
        avalibleDirection.Add(Vector3.up);
        avalibleDirection.Add(Vector3.left);
        avalibleDirection.Add(Vector3.down);
        avalibleDirection.Add(Vector3.right);

        List<RaycastHit2D> ray = new List<RaycastHit2D>();

        //shoot ray cast in 4 direction
        ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.up * gridSize, stopMask));
        ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.left * gridSize, stopMask));
        ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.down * gridSize, stopMask));
        ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.right * gridSize, stopMask));

        var choiceToBeRemove = new List<int>();

        //check for viable move
        for (int i = 0; i < 4; i++)
        {
            var rayTarget = transform.localPosition + avalibleDirection[i] * gridSize;
            newdistance = (int)(startingPoint - rayTarget).magnitude;

            if (ray[i] || newdistance > DistanceLimit)
            {
                choiceToBeRemove.Add(i);
            }
        }

        if (choiceToBeRemove.Count <= avalibleDirection.Count)
        {

            //Randomly choose a direction from the list
            if (avalibleDirection != null)
            {
                var r = Random.Range(0, avalibleDirection.Count);

                do
                {
                    r = Random.Range(0, avalibleDirection.Count);
                }
                while (choiceToBeRemove.Contains(r));

                Move(avalibleDirection[r]);
            }
        }

        ray.Clear();
        avalibleDirection.Clear();
        choiceToBeRemove.Clear();
    }
}
