using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Warrior : Enemy
{
    [Tooltip("����ö�Թ�Ũҡ�ش����������ҡ���˹")]
    public int DistanceLimit;
    public int playerDetectRange;

    int currentdistance;

    Vector3 startingPoint;
    public Transform player;

    enum moveState { Roam, Chase }
    moveState currentMoveState = moveState.Roam;

    protected override void Start()
    {
        base.Start();

        if (player == null)
        player = GameObject.FindWithTag("Player").transform;

        startingPoint = transform.position;
    }

    protected override void WalkPattern()
    {
        var distanceFromPlayer = (int)(transform.position - player.transform.position).magnitude;

        if (distanceFromPlayer <= playerDetectRange)
            currentMoveState = moveState.Chase;

        List<Vector3> avalibleDirection = new List<Vector3>();
        avalibleDirection.Add(Vector3.up);
        avalibleDirection.Add(Vector3.left);
        avalibleDirection.Add(Vector3.down);
        avalibleDirection.Add(Vector3.right);

        List<RaycastHit2D> ray = new List<RaycastHit2D>();

        var choiceToBeRemove = new List<int>();

        switch (currentMoveState)
        {
            case moveState.Roam:

                #region Roam AI
                if (DistanceLimit == 0)
                    return;

                //shoot ray cast in 4 direction
                ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.up * gridSize, stopMask));
                ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.left * gridSize, stopMask));
                ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.down * gridSize, stopMask));
                ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.right * gridSize, stopMask));

                // Guards walk in a random manner but will try to stay in their zone

                currentdistance = (int)(startingPoint - transform.position).magnitude;

                var newdistance = 0;

                //check for viable move
                for (int i = 0; i < 4; i++)
                {
                    var rayTarget = transform.localPosition + avalibleDirection[i] * gridSize;
                    newdistance = (int)(startingPoint - rayTarget).magnitude;

                    if (ray[i] || newdistance > DistanceLimit)
                    {
                        choiceToBeRemove.Add(i);
                    }
                }

                if (choiceToBeRemove.Count <= avalibleDirection.Count)
                {
                    //Randomly choose a direction from the list
                    if (avalibleDirection != null)
                    {
                        var r = Random.Range(0, avalibleDirection.Count);

                        do
                        {
                            r = Random.Range(0, avalibleDirection.Count);
                        }

                        while (choiceToBeRemove.Contains(r));

                        Move(avalibleDirection[r]);
                    }
                }

                #endregion

                break;

            case moveState.Chase:

                #region Chase AI

                if (distanceFromPlayer > 0)
                {
                    var distanceToBeCompare = new List<int>();

                    //nominate first one on the list as "The Least"
                    var leastDistanceChoice = 0;

                    //shoot ray cast in 4 direction
                    ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.up * gridSize, stopMask));
                    ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.left * gridSize, stopMask));
                    ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.down * gridSize, stopMask));
                    ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.right * gridSize, stopMask));

                    //check for viable move, happen 3 time.
                    for (int i = 0; i < 3; i++)
                    {
                        var leastRayplayerDistance = 0;

                        //if there something in this ray way then skip this step, and choose next one as "The Least" if this is first time.
                        if (ray[i])
                        {
                            if (i == 0)
                                leastDistanceChoice = i + 1;
                            continue;
                        }

                        //if not then compare this ray and next ray distance from player.

                        leastRayplayerDistance = (int)((transform.localPosition + avalibleDirection[leastDistanceChoice] * gridSize) - player.transform.position).magnitude;
                        var nextRayplayerDistance = ((transform.localPosition + avalibleDirection[i + 1] * gridSize) - player.transform.position).magnitude;

                        print("step : " + (i + 1) + ", Compare : least " + leastRayplayerDistance + " and next " + nextRayplayerDistance);

                        if (leastRayplayerDistance < nextRayplayerDistance || ray[i + 1])
                        {
                            print("> least is lesser");
                        }
                        else if (leastRayplayerDistance == nextRayplayerDistance)
                        {
                            print("> least and next is equal");
                            leastDistanceChoice = Random.Range(leastDistanceChoice, i + 2);
                        }
                        else
                        {
                            print("> next is lesser");
                            leastDistanceChoice = i + 1;
                        }


                        print("> > step : " + (i + 1) + ", least choice : " + leastDistanceChoice);
                    }

                    print("> > > Final choice :" + leastDistanceChoice);

                    Move(avalibleDirection[leastDistanceChoice]);
                }
                #endregion

                break;
        }

        ray.Clear();
        avalibleDirection.Clear();
        choiceToBeRemove.Clear();
    }
                
}

