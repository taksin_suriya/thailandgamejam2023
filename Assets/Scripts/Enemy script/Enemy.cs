using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    public BattleManager battleM;
    public int chanceToLose = 50;
    public float actionInterval = 1;
    float actionTimer;
    Renderer rend;

    protected virtual void Start()
    {
        animator = GetComponent<Animator>();
        rend = GetComponent<Renderer>();

        if (battleM == null)
            battleM = FindAnyObjectByType<BattleManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("Enemy Triggered");

        if (currentSlate == state.Alive)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                print("touching player");

                collision.gameObject.TryGetComponent<Character>(out Character player);

                BattlePlayer(player);
            }

            if (collision.gameObject.layer == 10)
            {
                print("Ally Contacted");

                collision.gameObject.TryGetComponent<Character>(out Character allie);

                if (allie.currentSlate == state.Alive)
                {
                    BattlePlayerAllies(allie);
                }


            }
        }

    }

    protected override void Update()
    {
        actionTimer += Time.deltaTime;

        if (actionTimer > actionInterval && currentSlate == state.Alive)
        {
            WalkPattern();
            actionTimer = 0;
        }
    }


    protected virtual void WalkPattern()
    {

    }


    protected void BattlePlayer(Character player)
    {
        print("Player Contacted");

        battleM.StartBattle(player, this);
    }

    protected void BattlePlayerAllies(Character allie)
    {
        StartCoroutine(BattleCouroutine(allie));
    }

    protected virtual IEnumerator BattleCouroutine(Character allie)
    {
        print("battle begin");

        allie.changeState(state.Battle);
        changeState(state.Battle);
        //change sprites

        allie.gameObject.transform.LeanMove(transform.position, 1f).setEaseOutQuad();

        yield return new WaitForSeconds(2);
        var rng = Random.Range(1, 101);
        if (rng > chanceToLose)
        {
            allie.changeState(state.Die);
            changeState(state.Alive);
            animator.SetBool("isBattling", false);
        }
        else
        {
            allie.changeState(state.Alive);
            changeState(state.Die);
        }


    }


    protected override void OnStateChanged()
    {
        switch (currentSlate)
        {
            case state.Battle:
                AudioManager.Instance.PlaySFX(AudioManager.Instance.fightSFX);
                // When in the Battle state, set the "isBattling" parameter to true in the animator.
                animator.SetBool("isBattling", true);
                break;

            case state.Die:
                // When in the Die state:
                AudioManager.Instance.PlaySFX(AudioManager.Instance.winSFX);
                animator.SetBool("isBattling", false); // Turn off the "isBattling" parameter.

                // Play dead animation and disable the character:
                BoxCollider2D boxCollider2D = GetComponent<BoxCollider2D>();
                rend.material.color = Color.red; // Change material color.
                transform.eulerAngles = new Vector3(0, 0, 90); // Rotate the character.
                boxCollider2D.enabled = false; // Disable the collider.
                Destroy(animator, 3); // Destroy the animator component after 3 seconds (or as needed).
                break;
        }
    }
}
