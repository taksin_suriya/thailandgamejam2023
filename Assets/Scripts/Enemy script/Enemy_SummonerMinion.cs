using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_SummonerMinion : Enemy
{
    public int playerDetectRange;

    public Transform player;

    enum moveState { Idle, Surround }
    moveState currentMoveState = moveState.Idle;

    public enum surroundType { Random, SurroundNorth, SurroundEast, SurroundSouth }
    public surroundType currentSurroundType;

    protected override void Start()
    {
        base.Start();

        if (player == null)
            player = GameObject.FindWithTag("Player").transform;
    }

    protected override void WalkPattern()
    {
        var distanceFromPlayer = (int)(transform.position - player.transform.position).magnitude;
        var targetPos = Vector3.zero;

        if (distanceFromPlayer <= playerDetectRange)
        {
            currentMoveState = moveState.Surround;

            if (currentSurroundType == surroundType.Random)
                currentSurroundType = (surroundType)Random.Range(1, 4);

            switch (currentSurroundType)
            {
                case surroundType.SurroundNorth:
                    targetPos = player.transform.position + Vector3.up * gridSize;
                    break;
                case surroundType.SurroundEast:
                    targetPos = player.transform.position + Vector3.left * gridSize;
                    break;
                case surroundType.SurroundSouth:
                    targetPos = player.transform.position + Vector3.down * gridSize;
                    break;
            }
        }

        List<Vector3> avalibleDirection = new List<Vector3>();
        avalibleDirection.Add(Vector3.up);
        avalibleDirection.Add(Vector3.left);
        avalibleDirection.Add(Vector3.down);
        avalibleDirection.Add(Vector3.right);

        List<RaycastHit2D> ray = new List<RaycastHit2D>();

        var choiceToBeRemove = new List<int>();

        switch (currentMoveState)
        {
            case moveState.Idle:
                break;

            case moveState.Surround:

                #region Chase AI

                if ((transform.position - targetPos).magnitude > 0)
                {
                    var distanceToBeCompare = new List<int>();

                    //nominate first one on the list as "The Least"
                    var leastDistanceChoice = 0;

                    //shoot ray cast in 4 direction
                    ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.up * gridSize, stopMask));
                    ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.left * gridSize, stopMask));
                    ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.down * gridSize, stopMask));
                    ray.Add(Physics2D.Linecast(transform.localPosition, transform.localPosition + Vector3.right * gridSize, stopMask));

                    //check for viable move, happen 3 time.
                    for (int i = 0; i < 3; i++)
                    {
                        var leastRayplayerDistance = 0;

                        //if there something in this ray way then skip this step, and choose next one as "The Least" if this is first time.
                        if (ray[i])
                        {
                            if (i == 0)
                                leastDistanceChoice = i + 1;
                            continue;
                        }

                        //if not then compare this ray and next ray distance from player.

                        leastRayplayerDistance = (int)((transform.localPosition + avalibleDirection[leastDistanceChoice] * gridSize) - targetPos).magnitude;
                        var nextRayplayerDistance = ((transform.localPosition + avalibleDirection[i + 1] * gridSize) - targetPos).magnitude;

                        print("step : " + (i + 1) + ", Compare : least " + leastRayplayerDistance + " and next " + nextRayplayerDistance);

                        if (leastRayplayerDistance < nextRayplayerDistance || ray[i + 1])
                        {
                            print("> least is lesser");
                        }
                        else if (leastRayplayerDistance == nextRayplayerDistance)
                        {
                            print("> least and next is equal");
                            leastDistanceChoice = Random.Range(leastDistanceChoice, i + 2);
                        }
                        else
                        {
                            print("> next is lesser");
                            leastDistanceChoice = i + 1;
                        }


                        print("> > step : " + (i + 1) + ", least choice : " + leastDistanceChoice);
                    }

                    print("> > > Final choice :" + leastDistanceChoice);

                    Move(avalibleDirection[leastDistanceChoice]);
                }
                #endregion

                break;
        }

        ray.Clear();
        avalibleDirection.Clear();
        choiceToBeRemove.Clear();
    }

    protected override void OnStateChanged()
    {
        switch (currentSlate)
        {
            case state.Battle:
                animator.SetBool("isBattling", true);
                break;
            case state.Die:
                //play dead anim then disable
                //animator.SetBool("isBattling", false);
                gameObject.SetActive(false);
                break;
        }
    }
}
