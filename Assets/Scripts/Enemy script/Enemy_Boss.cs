using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy_Boss : Enemy
{
    public SceneTransition sceneTransition;
    protected override void OnStateChanged()
    {
        switch (currentSlate)
        {
            case state.Battle:
                animator.SetBool("isBattling", true);
                break;
            case state.Die:
                animator.SetBool("isBattling", false);
                StartCoroutine(BossDieProgress());
                Destroy(animator, 3);
                break;
        }
    }

    IEnumerator BossDieProgress()
    {
        sceneTransition.PlayTransition();
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
