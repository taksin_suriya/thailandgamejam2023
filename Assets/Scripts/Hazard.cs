using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hazard : MonoBehaviour
{
    public LayerMask destroyThisObject;

    // void OnCollisionEnter2D(Collision2D other)
    // {
    //     Debug.Log("Hit Hazard");
    //     if (other.gameObject.CompareTag("Monster_2") || other.gameObject.CompareTag("Monster_1"))
    //     {
    //         Debug.Log("It hit");
    //         Destroy(other.gameObject);
    //     }

    // }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Hit Hazard");
        if (other.gameObject.CompareTag("Monster_2") || other.gameObject.CompareTag("Monster_1"))
        {
            Debug.Log("It hit");
            Destroy(other.gameObject);
        }
        
    }

}
