using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GridMovement : Character
{
    public SceneTransition sceneTransition;


    private void Start()
    {
        if (gameObject.CompareTag("Monster_1") || gameObject.CompareTag("Monster_2"))
        {
            currentSlate = state.Die;
        }
    }

    protected override void Update()
    {
        base.Update();

        if (!isMoving)
        {
            if (Input.GetKey(KeyCode.W))
            {
                Move(Vector3.up);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                Move(Vector3.down);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                Move(Vector3.left);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                Move(Vector3.right);
            }
        }
    }

    protected override void OnStateChanged()
    {
        switch (currentSlate)
        {
            case state.Die:
                if (gameObject.CompareTag("Monster_1") || gameObject.CompareTag("Monster_2"))
                {
                    AudioManager.Instance.PlaySFX(AudioManager.Instance.deathSFX);
                    Destroy(gameObject);
                }
                else
                {
                    AudioManager.Instance.PlaySFX(AudioManager.Instance.loseSFX);
                    StartCoroutine(Die());
                }
                break;
            case state.Alive:
                Debug.Log("monster or player StateAlive");
                if (gameObject.CompareTag("Monster_1") || gameObject.CompareTag("Monster_2"))
                {
                    Debug.Log("Monster change isMoving False");
                    isMoving = false;
                }
                break;
        }
    }

    IEnumerator Die()
    {
        sceneTransition.PlayTransition();
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
