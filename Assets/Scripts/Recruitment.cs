using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class Recruitment : MonoBehaviour
{

    Rigidbody2D rb;
    PlayerInput playerInput;
    Animator animator;
    Renderer rend;
    GridMovement gridMovement;

    public bool canMove = false;


    private void Start()
    {
        // Get references to components in Start.
        playerInput = GetComponent<PlayerInput>();
        animator = GetComponent<Animator>();
        rend = GetComponent<Renderer>();
        rb = GetComponent<Rigidbody2D>();
        gridMovement = GetComponent<GridMovement>();

        // Check if rend is not null before setting its color.
        if (rend != null)
        {
            rend.material.color = Color.red;
        }

        // Initialize other properties.
        transform.eulerAngles = new Vector3(0, 0, 90);
        rb.bodyType = RigidbodyType2D.Static;
        animator.enabled = false;
        playerInput.enabled = false;
    }

    void Update()
    {
        if (!canMove)
        {
            playerInput.enabled = false;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player") && canMove == false)
        {
            Debug.Log("Triggered by player");

            if (playerInput != null)
            {
                // Change the color to white.
                if (rend != null)
                {
                    rend.material.color = Color.white;

                }
                else
                {
                    Debug.LogWarning("Renderer component is missing.");
                }

                // Enable player input and update other properties.
                //playerInput.enabled = true;
                transform.eulerAngles = new Vector3(0, 0, 0);
                animator.enabled = true;
                gridMovement.currentSlate = Character.state.Alive;
                canMove = true;
                AudioManager.Instance.PlaySFX(AudioManager.Instance.helpFriendSFX);
            }
            else
            {
                Debug.LogWarning("PlayerInput component is missing.");
            }
        }
    }
}

