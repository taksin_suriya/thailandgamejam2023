using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class OptionSetting : MonoBehaviour
{
    [SerializeField] private AudioMixer myMixer;
    [SerializeField] private Slider musicSlider;
    [SerializeField] private Slider SFXSlider;
    [SerializeField] private GameObject optionPanel;

    private AudioManager audioManager;



    private void Start() 
    {
        audioManager = FindObjectOfType<AudioManager>();
        optionPanel.SetActive(false);

        if(PlayerPrefs.HasKey("musicVolume"))
        {
            loadVolume();
        }
        else
        {
            setMusicVolume();
            setSFXVolume();
        }
    }

    void Update() 
    {
        TogglePanel();
    }

    public void setMusicVolume()
    {
        float volume = musicSlider.value;
        myMixer.SetFloat("music" ,Mathf.Log10(volume)*20);
        PlayerPrefs.SetFloat("musicVolume",volume);
    }

    public void setSFXVolume()
    {
        float volume = SFXSlider.value;
        myMixer.SetFloat("sfx" ,Mathf.Log10(volume)*20);
        PlayerPrefs.SetFloat("sfxVolume",volume);

        if (Input.GetMouseButtonDown(0))
        {
            //PlaySFX Here
        }
        
    }

    private void loadVolume()
    {
        musicSlider.value = PlayerPrefs.GetFloat("musicVolume");
        SFXSlider.value = PlayerPrefs.GetFloat("sfxVolume");

        setMusicVolume();
        setSFXVolume();
    }

    private void TogglePanel()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            optionPanel.SetActive(!optionPanel.activeSelf);
            Time.timeScale = optionPanel.activeSelf ? 0f : 1f; //// Pause and Resume the game
        }
    }

    public void Restaet()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        optionPanel.SetActive(false);
        Time.timeScale = 1f;
        audioManager.PlayMusic(audioManager.backgroundMusic);
        SceneManager.LoadScene(currentSceneIndex);
    }
}
