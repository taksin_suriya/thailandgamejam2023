using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CalculateDistance : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Transform king;
    private int distance;
    public TextMeshProUGUI distanceText;

    private void Update()
    {
        float floatDistance = Vector3.Distance(player.position, king.position); // Calculate the float distance
        distance = (int)floatDistance; // Cast the float distance to int
        distanceText.text = distance.ToString() + " m";
    }
}
